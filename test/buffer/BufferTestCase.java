package buffer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigInteger;
import java.util.Random;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import buffer.Buffer;

public class BufferTestCase {

	Buffer buffer;

	@Before
	public void setUp() throws Exception {
		buffer = new Buffer(5);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSizeSiEstaVacio() {
		assertTrue(this.buffer.isEmpty());
	}


	@Test
	public void testIsEmptySiEstaVacio() {
		assertTrue(this.buffer.isEmpty());
	}

	@Test
	public void testPopSiHayTresElementos() {
		this.buffer.push(BigInteger.valueOf(1));
		this.buffer.push(BigInteger.valueOf(2));
		this.buffer.push(BigInteger.valueOf(3));

		assertTrue(this.buffer.pop().equals(BigInteger.valueOf(1)));
		assertTrue(this.buffer.pop().equals(BigInteger.valueOf(2)));
		assertTrue(this.buffer.pop().equals(BigInteger.valueOf(3)));
		assertTrue(this.buffer.isEmpty());
	}
}
