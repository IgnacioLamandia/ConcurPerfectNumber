package threadpool;

import static org.junit.Assert.*;

import java.math.BigInteger;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import buffer.Buffer;
import threadPool.ThreadPool;

public class ThreadPoolTest {

	Buffer buffer;
	ThreadPool threadPool;
	
	@Before
	public void setUp() throws Exception {
		buffer = new Buffer(50);
		threadPool = new ThreadPool(buffer, 4);
		
	}

	@Test
	public void CienNumerosTest() {
		threadPool.startAllThreads();

		for(int i = 1; i<100; i++){
			buffer.push(BigInteger.valueOf(i));
		}
		
		ArrayList<BigInteger> perfectos = new ArrayList<BigInteger>();
		perfectos.add(BigInteger.valueOf(6));
		perfectos.add(BigInteger.valueOf(28));
		perfectos.add(BigInteger.valueOf(496));
		perfectos.add(BigInteger.valueOf(8128));
		perfectos.add(BigInteger.valueOf(33550336));
		
		
		buffer.push(new BigInteger("33550336"));
		buffer.push(new BigInteger("33550336"));
		buffer.push(BigInteger.valueOf(33550336));
		buffer.push(BigInteger.valueOf(496));
		buffer.push(BigInteger.valueOf(8128));
		buffer.push(BigInteger.valueOf(-2));
		buffer.push(BigInteger.valueOf(-2));
		buffer.push(BigInteger.valueOf(-1));
		buffer.push(BigInteger.valueOf(-2));
	}

}
