package barrier;

public class Barrier {
	private int n;
	private int esperando;
	private boolean saliendo;

	public Barrier(int n) {
		this.n = n;
		this.esperando = 0;
		this.saliendo = false;

	}

	public synchronized void esperar() {
		while (saliendo) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		entrarBarrera();
		if (esperando != n) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		salirBarrera();
	}

	public void entrarBarrera() {
		esperando++;
		if (esperando == n) {
			saliendo = true;
			notifyAll();
		}
	}

	public void salirBarrera() {
		esperando--;
		if (esperando == 0) {
			saliendo = false;
			notifyAll();
		}
	}
}