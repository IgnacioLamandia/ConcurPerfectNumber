package perfectWorker;

import java.math.BigInteger;

import barrier.Barrier;
import buffer.Buffer;
import threadPool.ThreadPool;

public class PerfectWorker extends Thread {

	private Buffer buffer;
	private Barrier barrier;
	private ThreadPool threadPool;

	public PerfectWorker(Buffer buffer, Barrier barrier, ThreadPool threadPool) {
		this.buffer = buffer;
		this.barrier = barrier;
		this.threadPool = threadPool;
	}

	@Override
	public void run() {

		System.out.println("Empezó: Id " + this.getId());

		this.barrier.esperar();
		while (true) {
			BigInteger numero = (BigInteger) buffer.pop();

			if (numero.compareTo(BigInteger.valueOf(0)) == -1) {
				this.barrier.esperar();
				System.out.println("Terminó: Id " + this.getId());
				threadPool.setTotalTime();
				break;
			}
			if (verificarSiEsPerfecto(numero)) {
				System.out.println("Numero perfecto: " + numero + " verificado por thread ID: " + getId());
				threadPool.addPerfectNumber(numero);
			}
		}
	}

	public boolean verificarSiEsPerfecto(BigInteger n) {
		BigInteger suma = BigInteger.valueOf(0);
		BigInteger i = BigInteger.valueOf(1);

		while (i.compareTo(n) != 0) {
			if (n.mod(i).equals(BigInteger.valueOf(0))) {
				suma = suma.add(i);
			}
			i = i.add(BigInteger.valueOf(1));
		}

		if (suma.equals(n)) {
			// System.out.println("El número " + n + " es perfecto");
			return true;
		} else {
			// System.out.println("El número " + n + " no es perfecto");
			return false;
		}
	}
}
