package buffer;

public class Buffer {

	private Object[] data;
	private int begin = 0, end = 0, N;

	public Buffer(int n) {
		this.N = n;
		this.data = new Object[N + 1];
	}

	public int getSize() {
		return N;
	}

	public Object[] getData() {
		return data;
	}

	public synchronized void push(Object o) {
		while (isFull()) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		data[begin] = o;
		begin = next(begin);
		notifyAll();
	}

	public synchronized Object pop() {

		while (isEmpty()) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		Object result = data[end];
		end = next(end);
		notifyAll();
		return result;
	}

	public boolean isEmpty() {
		return begin == end;
	}

	private boolean isFull() {
		return next(begin) == end;
	}

	private int next(int i) {
		return (i + 1) % (N + 1);
	}
}
