package main;

import java.math.BigInteger;
import buffer.Buffer;
import threadPool.ThreadPool;

public class Main {

	public static void main(String[] args) {

		Buffer buffer = new Buffer(1);

		ThreadPool threadPool = new ThreadPool(buffer, 4);
		threadPool.startAllThreads();

		for (int i = 1; i < 100; i++) {
			buffer.push(BigInteger.valueOf(i));
		}
		buffer.push(new BigInteger("33550336"));
		buffer.push(new BigInteger("33550336"));
		buffer.push(BigInteger.valueOf(33550336));
		buffer.push(BigInteger.valueOf(496));
		buffer.push(BigInteger.valueOf(8128));
		buffer.push(BigInteger.valueOf(-2));
		buffer.push(BigInteger.valueOf(-2));
		buffer.push(BigInteger.valueOf(-1));
		buffer.push(BigInteger.valueOf(-2));
	}
}
