package threadPool;

import java.math.BigInteger;
import java.time.Duration;
import java.time.LocalDateTime;

import barrier.Barrier;
import buffer.Buffer;
import perfectWorker.PerfectWorker;

public class ThreadPool {
	private LocalDateTime startDate;
	private LocalDateTime endDate;
	private Duration duration;
	private Buffer threads;
	private Buffer perfectNumbers;
	private boolean firstStart;

	public ThreadPool(Buffer buffer, int numberOfThreads) {
		firstStart = true;
		Barrier barrier = new Barrier(numberOfThreads);
		perfectNumbers = new Buffer(100);
		threads = new Buffer(numberOfThreads);
		for (int i = 0; i < numberOfThreads; i++) {
			threads.push(new PerfectWorker(buffer, barrier, this));
		}
	}

	public synchronized void startAllThreads() {
		setStartDate();
		for (int i = 0; i < threads.getSize(); i++) {
			((PerfectWorker) threads.getData()[i]).start();
		}
	}

	private synchronized void setStartDate() {
		if (firstStart) {
			startDate = LocalDateTime.now();
			firstStart = false;
		}
	}

	public synchronized void addPerfectNumber(BigInteger number) {
		perfectNumbers.push(number);
	}

	public synchronized void setTotalTime() {
		threads.pop();
		if (threads.isEmpty()) {
			endDate = LocalDateTime.now();
			duration = Duration.between(startDate, endDate);
			resultado();
		}
	}

	public Duration getDuration() {
		return duration;
	}

	public void resultado() {
		System.out.println("Tiempo transcurrido: " + getDuration().toMillis() + " milisegundos");
	}

}